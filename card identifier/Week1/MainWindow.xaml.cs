﻿//my group members are:
//Alina Biesiedina 8326118
//Sharon Ekunsumi 8448946
//Sanusha Galappathy 8639567
//Shruti Geeta 8632992
//Roman Onyshkevych 8326084
//Saurabh Sharda 8620847

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Week1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        // The back code specifies the result of a button click. For example, if you click the 2 of Clubs button, the label changes to read Two of Clubs. 
        private void TwoofClubs_Click(object sender, RoutedEventArgs e)
        {
            lblChanged.Content = "Two of Clubs";
        }

        private void TwoofDiamonds_Click(object sender, RoutedEventArgs e)
        {
            lblChanged.Content = "Two of Diamonds";
        }

        private void TwoofHearts_Click(object sender, RoutedEventArgs e)
        {
            lblChanged.Content = "Two of Hearts";
        }

        private void TwoofSpades_Click(object sender, RoutedEventArgs e)
        {
            lblChanged.Content = "Two of Spades";
        }

        private void ThreeofClubs_Click(object sender, RoutedEventArgs e)
        {
            lblChanged.Content = "Three of Clubs";
        }
    }
}
